/*
Programado por Eloi Casamayor Esteve.
*/

#include "curses.h"
#include <vector>
#include <windows.h>
#include <algorithm>
using namespace std;
int i, y=0, x=0, c=0;

/*
ARRAY TABLERO
En este array se guarda la informaci�n de las piezas que est�n en el tablero.
*/
int tablero[13][6]={
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0},
        {0,0,0,0,0,0}};
bool revision_completa=false;

/*
OBJETO PIEZA
Contiene posici�n x,y e un color (del 1 al 4)
*/
struct Pieza{
	int x, y, color;
};
std::vector<Pieza*> combo(1000);
std::vector<Pieza*> fichas_revisadas(1000);
std::vector<Pieza*> moverse_a(1000);

/*
OBJETO PAREJA
Contiene 2 piezas y una variable para saber la posici�n
*/
struct Pareja{
	Pieza *p[2];
	int pos;
};
void EscucharEventos(Pareja*);
//----------------------------------

void LaOtraPiezaCae(Pieza*);
int kbhit(void);
void NuevaBurbuja();
void pintarTablero();
void pintarBordes();
Pareja *GenerarNuevaPareja();
Pareja *pareja_activa;
bool LaPiezaCae(Pareja*);
void ComprobarCombo(Pieza*);
void RevisarFichasAlrededor(Pieza*);

/*
FUNCI�N "MAIN"
En esta funci�n se inicia el juego, y en esta est� el bucle principal del juego.
*/
int main(){
    initscr();

    cbreak();
    noecho();
    curs_set(0);
    start_color();
    pintarBordes();
    /*
    COLORES
    Seg�n el valor "color" que tenga cada pieza, se pintar� por pantalla de un color.
    */
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLUE, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    //---------------------
    nodelay(stdscr, TRUE);

    scrollok(stdscr, TRUE);

    LOOP:
    pareja_activa = GenerarNuevaPareja();
    while(LaPiezaCae(pareja_activa)){

        EscucharEventos(pareja_activa);
        move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
        printw(" ");
        tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x]=0;
        move(pareja_activa->p[1]->y,pareja_activa->p[1]->x);
        printw(" ");
        tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;

        pareja_activa->p[0]->y=(pareja_activa->p[0]->y)+1;
        move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
        attron(COLOR_PAIR(pareja_activa->p[0]->color));
        addch(219);
        attroff(COLOR_PAIR(pareja_activa->p[0]->color));
        tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x]=pareja_activa->p[0]->color;

        pareja_activa->p[1]->y=(pareja_activa->p[1]->y)+1;
        move(pareja_activa->p[1]->y,pareja_activa->p[1]->x);
        attron(COLOR_PAIR(pareja_activa->p[1]->color));
        addch(219);
        attroff(COLOR_PAIR(pareja_activa->p[1]->color));
        tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;
        pintarTablero();

        Sleep(50);
    }
    goto LOOP;
    return 0;
}

/*
FUNCI�N "LA PIEZA CAE"
Comprueba si la pieza lleg� al sueelo o si tiene una ficha a debajo.
En el caso de que una pieza de la pareja haya topado con una pieza y la otra no, se llama la funci�n "LaOtraPiezaCae" pasando como valor la pieza que no cae todav�a
*/

bool LaPiezaCae(Pareja* pareja_activa){

    if((pareja_activa->p[0]->y+1>12)&&(pareja_activa->p[1]->y)+1>12){ //Si una de las dos piezas llega al suelo (las dos llegan)
            ComprobarCombo(pareja_activa->p[0]);
            ComprobarCombo(pareja_activa->p[1]);
        return false;
    }
    if((tablero[pareja_activa->p[0]->y+1][pareja_activa->p[0]->x]!=0)&&(tablero[pareja_activa->p[1]->y+1][pareja_activa->p[1]->x]!=0)){ //Si una de las dos piezas llega al suelo (las dos llegan)
            ComprobarCombo(pareja_activa->p[0]);
            ComprobarCombo(pareja_activa->p[1]);
        return false;
    }
    if((pareja_activa->pos==1)||(pareja_activa->pos==3)){//posici�n horizontal
        if((tablero[pareja_activa->p[0]->y+1][pareja_activa->p[0]->x]!=0)&&(tablero[pareja_activa->p[1]->y+1][pareja_activa->p[1]->x]==0)){ //Si p[0] tiene abajo otra pieza
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
                ComprobarCombo(pareja_activa->p[0]);
                move(pareja_activa->p[1]->y,pareja_activa->p[1]->x);
                printw(" ");
                LaOtraPiezaCae(pareja_activa->p[1]);
                ComprobarCombo(pareja_activa->p[1]);
        return false;
        }
        if((tablero[pareja_activa->p[0]->y+1][pareja_activa->p[0]->x]==0)&&(tablero[pareja_activa->p[1]->y+1][pareja_activa->p[1]->x]!=0)){ //Si p[1] tiene abajo otra pieza
                tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x]=0;
                ComprobarCombo(pareja_activa->p[1]);
                move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
                printw(" ");
                LaOtraPiezaCae(pareja_activa->p[0]);
                ComprobarCombo(pareja_activa->p[0]);
        return false;
        }
        if((tablero[pareja_activa->p[0]->y+1][pareja_activa->p[0]->x]!=0)&&(tablero[pareja_activa->p[1]->y+1][pareja_activa->p[1]->x]!=0)){ //Si una de las dos piezas tiene abajo otra pieza
        return false;
        }
    }
    if(pareja_activa->pos==2){//posici�n vertical
        if(tablero[(pareja_activa->p[0]->y)+1][pareja_activa->p[0]->x]!=0){ //Si la pieza de abajo tiene abajo otra pieza
        ComprobarCombo(pareja_activa->p[0]);
        return false;
        }
    }
    if(pareja_activa->pos==4){//posici�n vertical
        if(tablero[(pareja_activa->p[1]->y)+1][pareja_activa->p[1]->x]!=0){ //Si la pieza de abajo tiene abajo otra pieza
        ComprobarCombo(pareja_activa->p[1]);
        return false;
        }
    }
    if(tablero[(pareja_activa->p[0]->y)+1][pareja_activa->p[0]->x]==0){ //Si todav�s siguen cayendo las 2 piezas
        return true;
    }
}

/*
FUNCI�N COMPROBARCOMBO
Comprueba si la pieza que cayo tiene tres piezas mas de su mismo color contiguas en horizontal o vertical.
*/
void ComprobarCombo(Pieza* p){
    revision_completa=false;
    combo.clear();
    fichas_revisadas.clear();
    moverse_a.clear();
    move(17,0);

    while(revision_completa==false){
        int contador=0;
        RevisarFichasAlrededor(p);
        move(15,0);printw("moverse_a size= %d //contador=%d", moverse_a.size(),contador);

        if(moverse_a.size()>0){
            MOVERSE:
            for(int i=0; i<moverse_a.size(); i++){
                p=moverse_a.at(i);
                RevisarFichasAlrededor(p);
            }
            /*for(std::vector<Pieza*>::iterator it = moverse_a.begin(); it!=moverse_a.end(); ++it){
                p=(*it);
                RevisarFichasAlrededor(p);
                a++;
                move(i,30);printw("MOVERSE_A-->%d<--",a);
            }*/
            if(moverse_a.size()>0){
                move(19,0);printw("MOVERSE VECTOR:::");
                for (std::vector<Pieza*>::iterator it = moverse_a.begin(); it!=moverse_a.end(); ++it) {
                        printw("(y:%d x:%d c:%d) ", (*it)->y, (*it)->x, (*it)->color);
                }
                /*
                _________________________________________
                �ESTA ES LA L�NEA QUE ME CAUSA EL ERROR!
                No he conseguido entender porqu� se para el juego cuando se produce una pareja, si la descomento.
                Mi idea es que, siempre que queden piezas por checar, seguir� el bucle.
                Pero se produce un bucle infinito y se para el programa.
                Entiendo que debe ser porque, estoy alterando el vector mientras lo estoy iterando, lo cu�l resulta en un bucle infinito.
                No he conseguido solucionarlo.
                :'(
                _________________________________________
                */
                //goto MOVERSE;
            }
            revision_completa=true;combo.clear();moverse_a.clear();
        }else{
            revision_completa=true;;combo.clear();moverse_a.clear();
            }
    }
}

/*
FUNCI�N "NUEVA PIEZA"
Simplemente crea una nueva pieza dadas una y, x y un color
*/
Pieza* NuevaPieza(int y, int x, int color){
    Pieza *p = new Pieza();
    p->y=y;
    p->x=x;
    p->color=color;
    return p;
}

/*
FUNCI�N "REVISAR FICHAS ALREDEDOR"
Se encarga de revisar si la pieza p tiene piezas contiguas del mismo color.
En esta funci�n se usan tres vectores:
->vector combo: aqu� colocamos la pieza p en el caso que tenga piezas contiguas del mismo color.
->vector fichas_revisadas: en este vector ponemos las piezas contiguas a p que posteriormente queremos revisar.
->vector moverse_a: es una copia de fichas_revisadas. Lo usamos para poder iterar sobre �l, ya que el fichas_revisadas tiene que sobreescribirse cada vez.
*/
void RevisarFichasAlrededor(Pieza*p){
    bool ficha_encontrada=false;
    //bool p_en_combo=false;
    move(0,20);printw("              ");move(1,20);printw("              ");move(2,20);printw("              ");move(3,20);printw("              ");
    fichas_revisadas.clear();
    if(tablero[p->y][p->x]==tablero[p->y][(p->x)-1]){
        if(std::find(combo.begin(), combo.end(), p) != combo.end()) { // si combo contiene p
        } else { //si combo no contiene p
            combo.push_back (p);
                move(0,20);
                attron(COLOR_PAIR(p->color));
                printw("PAREJA x-1", p->color);
                attroff(COLOR_PAIR(p->color));
            Pieza* pieza_colindante=NuevaPieza(p->y, (p->x)-1, tablero[p->y][(p->x)-1]);
            if(std::find(combo.begin(), combo.end(), pieza_colindante) != combo.end()){// si combo contiene pieza_colindante
                attron(COLOR_PAIR(2));
                printw("COLINDANTE YA ESTABA EN COMBO");
                attroff(COLOR_PAIR(2));
            }else{
                fichas_revisadas.push_back (pieza_colindante);
                ficha_encontrada=true;
            }
        }
    }
    if(tablero[p->y][p->x]==tablero[p->y][(p->x)+1]){
		if(std::find(combo.begin(), combo.end(), p) != combo.end()) { // si combo contiene p
        } else { //si combo no contiene p
            combo.push_back (p);
                move(1,20);
                attron(COLOR_PAIR(p->color));
                printw("PAREJA x+1");
                attroff(COLOR_PAIR(p->color));
            Pieza* pieza_colindante=NuevaPieza(p->y, (p->x)+1, tablero[p->y][(p->x)+1]);

            if(std::find(combo.begin(), combo.end(), pieza_colindante) != combo.end()){// si combo contiene pieza_colindante
                attron(COLOR_PAIR(2));
                printw("COLINDANTE YA ESTABA EN COMBO");
                attroff(COLOR_PAIR(2));
            }else{
                fichas_revisadas.push_back (pieza_colindante);
                ficha_encontrada=true;
            }
        }
    }
    if(tablero[p->y][p->x]==tablero[(p->y)-1][p->x]){
        if(std::find(combo.begin(), combo.end(), p) != combo.end()) { // si combo contiene p
        } else { //si combo no contiene p
            combo.push_back (p);
                move(2,20);
                attron(COLOR_PAIR(p->color));
                printw("PAREJA y-1", p->color);
                attroff(COLOR_PAIR(p->color));
            Pieza* pieza_colindante=NuevaPieza((p->y)-1, p->x, tablero[(p->y)-1][p->x]);
            if(std::find(combo.begin(), combo.end(), pieza_colindante) != combo.end()){
            }else{
                fichas_revisadas.push_back (pieza_colindante);
                ficha_encontrada=true;
            }
        }
    }
    if(tablero[p->y][p->x]==tablero[(p->y)+1][p->x]){
        if(std::find(combo.begin(), combo.end(), p) != combo.end()) { // si combo contiene p
        } else { //si combo no contiene p
            combo.push_back (p);
                move(3,20);
                attron(COLOR_PAIR(p->color));
                printw("PAREJA y+1", p->color);
                attroff(COLOR_PAIR(p->color));
            Pieza* pieza_colindante=NuevaPieza((p->y)+1, p->x, tablero[(p->y)+1][p->x]);
            if(std::find(combo.begin(), combo.end(), pieza_colindante) != combo.end()){ //si combo contiene pieza_colindante
            }else{
                fichas_revisadas.push_back (pieza_colindante);
                ficha_encontrada=true;
            }
        }
    }

    if(combo.size()>=4){printw("COMMBOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO!!!");
    revision_completa=true;
    combo.clear();}

    move(18,0);printw(" COMBO-> ");
    for (std::vector<Pieza*>::iterator it = combo.begin(); it!=combo.end(); ++it) {
            printw("(y%d x%d c%d) ", (*it)->y, (*it)->x, (*it)->color);
    }
    printw("\n FICHAS_R-> ");
    for (std::vector<Pieza*>::iterator it = fichas_revisadas.begin(); it!=fichas_revisadas.end(); ++it) {
            printw("(y%d x%d c%d) ", (*it)->y, (*it)->x, (*it)->color);
    }

    //moverse_a.insert(moverse_a.end(), fichas_revisadas.begin(), fichas_revisadas.end());


    moverse_a=fichas_revisadas;
        //moverse_a.insert(moverse_a.end(), fichas_revisadas.begin(), fichas_revisadas.end());

    printw("\n moverse_a");
    for (std::vector<Pieza*>::iterator it = moverse_a.begin(); it!=moverse_a.end(); ++it) {
            printw("(y:%d x:%d c:%d) ", (*it)->y, (*it)->x, (*it)->color);
    }
    printw("\n moverse_a.size()= %d",moverse_a.size());
    fichas_revisadas.clear();
}

/*
FUNCI�N "LA OTRA PIEZA CAE"
Funci�n que hace caer a una pieza cuando su pareja ya ha topado con una pieza.
*/
void LaOtraPiezaCae(Pieza* p){
    //printw("%d, %d, %d",y,x,c);
    while((tablero[p->y+1][p->x]==0)&&(p->y<12)){
        p->y=p->y+1;
    }
    tablero[p->y][p->x]=p->color;
    move(p->y,p->x);
    attron(COLOR_PAIR(p->color));
    addch(219);
    attroff(COLOR_PAIR(p->color));

}

/*
FUNCI�N "ESCUCHAR EVENTOS"
Funci�n que mira qu� tecla est� presionada.
->"A": mueve la pareja a la derecha
->"D": mueve la pareja a la izquierda
->"S": mueve la pareja hacia abajo
->"W": rota la pareja. hay una variable pos (puede ser 1, 2, 3 � 4) que guarda la posici�n.
*/
void EscucharEventos(Pareja* pareja_activa){
    i=0;
    //pareja_activa = main->pareja_activa;
    while(i<50){
    if (kbhit()) {

        int key=getch();
            move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
            printw(" ");
            move(pareja_activa->p[1]->y,pareja_activa->p[1]->x);
            printw(" ");
            tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x]=0;
            tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
           if(key==97){
                if(pareja_activa->p[0]->x>0){
                    if(tablero[pareja_activa->p[0]->y][(pareja_activa->p[0]->x)-1]==0){
                        pareja_activa->p[0]->x=pareja_activa->p[0]->x-1;
                        pareja_activa->p[1]->x=pareja_activa->p[1]->x-1;
                    }
                }
            }
            if(key==100){
                if(pareja_activa->p[1]->x<5){
                    if(tablero[y][x+1]==0){
                        pareja_activa->p[0]->x=pareja_activa->p[0]->x+1;
                        pareja_activa->p[1]->x=pareja_activa->p[1]->x+1;
                    }
                }
            }
           if(key==115){
                i=49;
           }
           if(key==119){
            if(pareja_activa->pos==1){
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
                pareja_activa->p[1]->x=pareja_activa->p[0]->x;
                pareja_activa->p[1]->y=pareja_activa->p[0]->y-1;
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;
                pareja_activa->pos=2;
            }else
            if((pareja_activa->pos==2)&&(tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x-1]==0)&&(pareja_activa->p[0]->x>0)){
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
                pareja_activa->p[1]->x=pareja_activa->p[0]->x-1;
                pareja_activa->p[1]->y=pareja_activa->p[0]->y;
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;
                pareja_activa->pos=3;
            }else
            if((pareja_activa->pos==3)&&((tablero[pareja_activa->p[0]->y-1][pareja_activa->p[0]->x]==0))&&(pareja_activa->p[0]->y<12)){
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
                pareja_activa->p[1]->x=pareja_activa->p[0]->x;
                pareja_activa->p[1]->y=pareja_activa->p[0]->y+1;
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;
                pareja_activa->pos=4;
            }else
            if((pareja_activa->pos==4)&&(pareja_activa->p[0]->x+1<6)&&(tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x+1]==0)){
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=0;
                pareja_activa->p[1]->x=pareja_activa->p[0]->x+1;
                pareja_activa->p[1]->y=pareja_activa->p[0]->y;
                tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;
                pareja_activa->pos=1;
            }

           }
            move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
            attron(COLOR_PAIR(pareja_activa->p[0]->color));
            printw("%c",219);
            attroff(COLOR_PAIR(pareja_activa->p[0]->color));
            tablero[pareja_activa->p[0]->y][pareja_activa->p[0]->x]=pareja_activa->p[0]->color;

            move(pareja_activa->p[1]->y,pareja_activa->p[1]->x);
            attron(COLOR_PAIR(pareja_activa->p[1]->color));
            printw("%c",219);
            attroff(COLOR_PAIR(pareja_activa->p[1]->color));
            tablero[pareja_activa->p[1]->y][pareja_activa->p[1]->x]=pareja_activa->p[1]->color;


            //printw("tablero[%d][%d]=%d",x,y,tablero[x][y]);
            move(pareja_activa->p[0]->y,pareja_activa->p[0]->x);
            //addch(219);
            //printw("HOLa:%d\n", ch);
            refresh();
            i++;
            pintarTablero();
            //move(60,60);
            //printw("HOLa:%d\n", key);
            Sleep(2);
        } else {
            //printw("No key pressed yet...\n");
            //refresh();
            i++;
            Sleep(2);
        }
    }
    //printw("i=%d sacabat lo puto temps", i);
    refresh();
}

/*
FUNCI�N "PINTAR TABLERO"
Esta funci�n es solo para debugging. Lo que hace es pintar el array de tablero[y][x]
*/
void pintarTablero(){
    for(int cordx=0;cordx<6;cordx++){
            for(int cordy=0;cordy<=12;cordy++){
                move(cordy, cordx+13);
                printw("%d",tablero[cordy][cordx]);
            }
    }
    refresh();
}

/*
FUNCI�N "GENERAR NUEVA PAREJA"
Esta funci�n genera una nueva pareja arriba del tablero. Se llama cuando las piezas "viejas" llegaron a abajo.
*/
Pareja *GenerarNuevaPareja(){

    Pareja *c = new Pareja();
    c->pos=1;
    for(int a = 0; a < 2; a++){
		Pieza *p = new Pieza();

		p->color = 1+rand()%4;
		p->y = 0;
		c->p[a] = p;
	}
    c->p[0]->x = 2;
	c->p[1]->x = 3;

	return c;

    /*x=3;y=0;
    move(y,x);
    c=1+rand()%4;
    attron(COLOR_PAIR(c));
    addch(219);
    attroff(COLOR_PAIR(c));*/
}

void pintarBordes(){
    for(int by=0;by<=12;by++){
        move(by,6);
        addch(219);
    }
}

/*
FUNCI�N "KBHIT"
Funci�n para saber si est� presionada una tecla
*/
int kbhit(void)
{
    int ch = getch();
    if (ch != ERR) {
            //printw("%c",ch);
        ungetch(ch);
        return 1;
    } else {
        return 0;
    }
}


